.PHONY: setup
setup: development-setup-env install-dependencies

.PHONY: run-dev
run-dev: setup
	docker-compose up

.PHONY: compose-build
compose-build:
	docker-compose build

.PHONY: install-dependencies
install-dependencies:
	docker-compose run task_manager npm install
	docker-compose run hook_parser npm install
	docker-compose run messenger npm install
	docker-compose run messenger npm run migrate:up

.PHONY: clean
clean:
	docker-compose down

.PHONY: visualize
visualize:
	make -Bnd | make2graph | dot -Tpng -o out.png
	open out.png

.PHONY: development-setup-env
development-setup-env:
	ansible-playbook ansible/development.yml -i ansible/development -vv

.PHONY: test
test: setup
	docker-compose -f docker-compose.test.yml up --exit-code-from task_manager

.PHONY: lint
lint: lint-task-manager lint-hook-parser lint-messenger

.PHONY: lint-task-manager
lint-task-manager:
	cd task-manager && npm run lint

.PHONY: lint-hook-parser
lint-hook-parser:
	cd hook-parser && npm run lint

.PHONY: lint-messenger
lint-messenger:
	cd messenger && npm run lint

