import * as Fastify from 'fastify';

const fastify = Fastify({ logger: true });

interface Body {
  typeChange?: string;
  typeData?: string;
  source?: string;
  data?: object;
}

const opts: Fastify.RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      properties: {
        typeChange: {
          type: 'string',
        },
        typeData: {
          type: 'string',
        },
        source: {
          type: 'string',
        },
        data: {
          type: 'object',
        },
      },
    },
  },
};

const buildFastify = () => {
  fastify.post('/', opts, async (request, reply) => {
    // tslint:disable-next-line: no-console
    console.log(request.body);
    reply.send(200);
  });

  return fastify;
};

export default buildFastify;
