import buildFastify from './app';

const app = buildFastify();

try {
  app.listen(
    Number(process.env.PORT_HOOK_PARSER),
    process.env.HOST_HOOK_PARSER,
  );
} catch (err) {
  app.log.error(err);
  process.exit(1);
}
