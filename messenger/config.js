import dotenv from 'dotenv';

const result = dotenv.config();

if (result.error) {
  throw result.error;
}

const {
  PORT_MESSENGER: portServer,
  NODE_ENV: environment,
  SECRET_KEY: secretKey,
  MONGO_URL: mongoUrl,
  TOKEN_COOKIE: tokenCookie,
} = result.parsed;

export default {
  portServer,
  environment,
  secretKey,
  mongoUrl,
  tokenCookie,
};
