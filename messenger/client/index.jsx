import '@babel/polyfill';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import 'open-iconic/font/css/open-iconic-bootstrap.css';
import ReactDOM from 'react-dom';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { keyBy } from 'lodash';
import io from 'socket.io-client';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { Start } from './components';
import * as actions from './actions';
import reducers from './reducers';

/* eslint-disable no-underscore-dangle */
const ext = window.__REDUX_DEVTOOLS_EXTENSION__;
const devtoolMiddleware = (ext && ext()) || compose;
const stateALT = window.__PRELOADED_STATE__;
/* eslint-enable */

const initState = {
  channels: keyBy(stateALT.channels, 'id'),
  messages: keyBy(stateALT.messages, 'id'),
  users: keyBy(stateALT.users, 'id'),
  currentChannelId: {
    current: stateALT.currentChannelId,
    default: stateALT.currentChannelId,
  },
  authUser: stateALT.authUser,
};
const store = createStore(
  reducers,
  initState,
  compose(applyMiddleware(thunk), devtoolMiddleware)
);
// -------------
const socket = io();

socket.on('newMessage', ({ data: { attributes } }) => {
  store.dispatch(actions.addMessageSuccess(attributes));
});
socket.on('newChannel', ({ data: { attributes } }) => {
  console.log('in jsx', attributes);
  if (
    attributes.users
      .map(({ username }) => username)
      .includes(stateALT.authUser.username)
  ) {
    store.dispatch(actions.addChannelSuccess(attributes));
  }
});
socket.on('removeChannel', ({ data }) => {
  console.log('data', data);
  store.dispatch(actions.deleteChannelSuccess(data));
});
socket.on('renameChannel', ({ data }) => {
  store.dispatch(actions.renameChannelSuccess(data));
});
socket.on('addUserToChannel', ({ data }) => {
  store.dispatch(actions.addUserToChannelSuccess(data));
});

socket.on('registerNewUser', ({ data }) => {
  store.dispatch(actions.registerNewUserSuccess(data));
});
// -------------

if (process.env.NODE_ENV !== 'production') {
  localStorage.debug = 'chat:*';
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Start />
    </BrowserRouter>
  </Provider>,
  document.getElementById('chat')
);
