import { keyBy, omit, omitBy } from 'lodash';
import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import { reducer as formReducer } from 'redux-form';
import * as actions from '../actions';

const currentChannelId = handleActions(
  {
    [actions.setCurrentChannnelId](state, { payload: channelId }) {
      console.log('in reduser, setCurrentChannnelId', state, channelId);
      return {
        ...state,
        current: channelId,
      };
    },
    // TODO for delete channel dont work set current channel
    [actions.deleteChannelSuccess](state) {
      return {
        current: state.default,
        default: state.default,
      };
    },
    [actions.loadStateSuccess](
      state,
      { payload: { currentChannelId: currentChannelIdLoad } }
    ) {
      console.log('in reduser, loadStateSuccess', state, currentChannelIdLoad);
      return {
        current: currentChannelIdLoad,
        default: currentChannelIdLoad,
      };
    },
    [actions.logoutSuccess]() {
      return {};
    },
  },
  {}
);

const channels = handleActions(
  {
    [actions.addChannelSuccess](state, { payload }) {
      return { ...state, [payload.id]: payload };
    },
    [actions.deleteChannelSuccess](state, { payload }) {
      return omit(state, payload.id);
    },
    [actions.renameChannelSuccess](state, { payload: { id, attributes } }) {
      return { ...state, [id]: attributes };
    },
    [actions.addUserToChannelSuccess](state, { payload: { id, attributes } }) {
      return { ...state, [id]: attributes };
    },
    [actions.loadStateSuccess](state, { payload: { channels: channelsLoad } }) {
      return keyBy(channelsLoad, 'id');
    },
    [actions.logoutSuccess]() {
      return {};
    },
  },
  {}
);

const messages = handleActions(
  {
    [actions.addMessageSuccess](state, { payload }) {
      return { ...state, [payload.id]: payload };
    },
    [actions.deleteChannelSuccess](state, { payload }) {
      const { id } = payload;
      return omitBy(state, { channelId: id });
    },
    [actions.loadStateSuccess](state, { payload: { messages: messagesLoad } }) {
      return keyBy(messagesLoad, 'id');
    },
    [actions.logoutSuccess]() {
      return {};
    },
  },
  {}
);

const users = handleActions(
  {
    [actions.loadUsers](state) {
      return state;
    },
    [actions.registerNewUserSuccess](state, { payload: { id, attributes } }) {
      return { ...state, [id]: attributes };
    },
    [actions.loadStateSuccess](state, { payload: { users: usersLoad } }) {
      return keyBy(usersLoad, 'id');
    },
    [actions.logoutSuccess]() {
      return {};
    },
  },
  {}
);

const authUser = handleActions(
  {
    [actions.loadAuthUser](state) {
      return state;
    },
    [actions.loadCurrentUserSuccess](state, { payload }) {
      console.log('in reduser', payload);
      return { userId: '', username: '' };
    },
    [actions.loadStateSuccess](state, { payload: { authUser: user } }) {
      return user;
    },
    [actions.logoutSuccess]() {
      return {};
    },
  },
  {}
);

export default combineReducers({
  currentChannelId,
  messages,
  channels,
  users,
  authUser,
  form: formReducer,
});
