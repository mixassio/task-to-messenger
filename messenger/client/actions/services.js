import axios from 'axios';
import routes from '../apiRoutes';
import {
  addMessageRequest,
  addMessageFailure,
  loadCurrentUserRequest,
  loadCurrentUserSuccess,
  loadCurrentUserFailure,
  loadStateRequest,
  loadStateSuccess,
  loadStateFailure,
  logoutRequest,
  logoutSuccess,
  logoutFailure,
  addChannelRequest,
  addChannelFailure,
  deleteChannelRequest,
  deleteChannelFailure,
  renameChannelRequest,
  renameChannelFailure,
  addUserToChannelRequest,
  addUserToChannelFailure,
  registerNewUserRequest,
} from './index';

export const addMessage = ({
  message,
  channelId,
  userId,
}) => async dispatch => {
  dispatch(addMessageRequest());
  try {
    const url = routes.message(channelId);
    const data = {
      attributes: { text: message, userId },
    };
    await axios.post(url, { data });
  } catch (e) {
    dispatch(addMessageFailure());
    throw e;
  }
};

export const loadCurrentUser = ({ email, password }) => async dispatch => {
  dispatch(loadCurrentUserRequest());
  try {
    const url = routes.createSession();
    const data = { email, password };
    const res = await axios.post(url, data);
    const { data: userId } = res;
    dispatch(loadCurrentUserSuccess(userId));
    return userId;
  } catch (e) {
    dispatch(loadCurrentUserFailure());
    throw e;
  }
};

export const loadState = () => async dispatch => {
  dispatch(loadStateRequest());
  try {
    const url = routes.getState();
    const { data } = await axios.post(url);
    dispatch(loadStateSuccess(data));
  } catch (e) {
    dispatch(loadStateFailure());
    throw e;
  }
};

export const logout = () => async dispatch => {
  dispatch(logoutRequest());
  try {
    const url = routes.logout();
    await axios.post(url);
    dispatch(logoutSuccess());
  } catch (e) {
    dispatch(logoutFailure());
    throw e;
  }
};

export const addChannel = name => async dispatch => {
  dispatch(addChannelRequest());
  try {
    const url = routes.channel();
    const data = {
      attributes: { name },
    };
    await axios.post(url, { data });
  } catch (e) {
    dispatch(addChannelFailure());
    throw e;
  }
};

export const deleteChannel = id => async dispatch => {
  dispatch(deleteChannelRequest());
  try {
    const url = routes.channels(id);
    await axios.delete(url);
  } catch (e) {
    dispatch(deleteChannelFailure());
    throw e;
  }
};

export const renameChannel = ({ channelId, name }) => async dispatch => {
  dispatch(renameChannelRequest());
  try {
    const url = routes.channels(channelId);
    const data = {
      attributes: {
        name,
      },
    };
    await axios.patch(url, { data });
  } catch (e) {
    dispatch(renameChannelFailure());
    throw e;
  }
};

export const addUserToChannel = ({ channelId, userId }) => async dispatch => {
  dispatch(addUserToChannelRequest());
  try {
    const url = routes.usersChannels(channelId);
    const data = {
      attributes: {
        userId,
      },
    };
    await axios.post(url, { data });
  } catch (e) {
    dispatch(addUserToChannelFailure());
    throw e;
  }
};

export const registerNewUser = fields => async dispatch => {
  dispatch(registerNewUserRequest());
  try {
    const url = routes.register();
    await axios.post(url, fields);
  } catch (e) {
    dispatch(addUserToChannelFailure());
    throw e;
  }
};
