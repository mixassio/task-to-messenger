import { createAction } from 'redux-actions';

export const addMessageRequest = createAction('MESSAGE_ADD_REQUEST');
export const addMessageSuccess = createAction('MESSAGE_ADD_SUCCESS');
export const addMessageFailure = createAction('MESSAGE_ADD_FAILURE');

export const setCurrentChannnelId = createAction('SET_CHANNEL_ID');

export const loadUsers = createAction('LOAD_USERS');
export const loadAuthUser = createAction('LOAD_AUTH_USER');

export const loadCurrentUserRequest = createAction('LOAD_TOKEN_REQUEST');
export const loadCurrentUserSuccess = createAction('LOAD_TOKEN_SUCCESS');
export const loadCurrentUserFailure = createAction('LOAD_TOKEN_FAILURE');

export const addChannelRequest = createAction('CHANNEL_ADD_REQUEST');
export const addChannelSuccess = createAction('CHANNEL_ADD_SUCCESS');
export const addChannelFailure = createAction('CHANNEL_ADD_FAILURE');

export const deleteChannelRequest = createAction('CHANNEL_DELETE_REQUEST');
export const deleteChannelSuccess = createAction('CHANNEL_DELETE_SUCCESS');
export const deleteChannelFailure = createAction('CHANNEL_DELETE_FAILURE');

export const renameChannelRequest = createAction('CHANNEL_RENAME_REQUEST');
export const renameChannelSuccess = createAction('CHANNEL_RENAME_SUCCESS');
export const renameChannelFailure = createAction('CHANNEL_RENAME_FAILURE');

export const addUserToChannelRequest = createAction(
  'ADD_USER_TO_CHANNEL_REQUEST'
);
export const addUserToChannelSuccess = createAction(
  'ADD_USER_TO_CHANNEL_SUCCESS'
);
export const addUserToChannelFailure = createAction(
  'ADD_USER_TO_CHANNEL_FAILURE'
);

export const loadStateRequest = createAction('LOAD_STATE_REQUEST');
export const loadStateSuccess = createAction('LOAD_STATE_SUCCESS');
export const loadStateFailure = createAction('LOAD_STATE_FAILURE');

export const logoutRequest = createAction('LOGOUT_REQUEST');
export const logoutSuccess = createAction('LOGOUT_SUCCESS');
export const logoutFailure = createAction('LOGOUT_FAILURE');

export const registerNewUserRequest = createAction('REG_NEW_USER_REQUEST');
export const registerNewUserSuccess = createAction('REG_NEW_USER_SUCCESS');
export const registerNewUserFailure = createAction('REG_NEW_USER_FAILURE');
