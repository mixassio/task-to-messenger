import React from 'react';
import { connect } from 'react-redux';
import { useLocation, Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { logout } from '../actions/services';

const mapDispatchToProps = { logout };

const mapStateToProps = ({ authUser }) => ({
  authUser,
});

const NavHeader = ({ authUser: { username }, logout: logoutAction }) => {
  const { pathname } = useLocation();

  const logoutButton = () => {
    try {
      logoutAction();
    } catch (err) {
      console.log(err);
    }

    return <Redirect to="/login" />;
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <Link className="navbar-brand nav-link" to="/">
        Messenger
      </Link>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/profile">
              {username}
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav">
          {pathname === '/' && (
            <li className="nav-item ">
              <button
                type="button"
                className="nav-link btn btn-link"
                onClick={logoutButton}
              >
                Sign Out
              </button>
            </li>
          )}
          {pathname === '/registration' && (
            <li className="nav-item ">
              <Link className="nav-link" to="/login">
                Sign In
              </Link>
            </li>
          )}
          {pathname === '/login' && (
            <li className="nav-item ">
              <Link className="nav-link" to="/registration">
                Registration
              </Link>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(NavHeader);
