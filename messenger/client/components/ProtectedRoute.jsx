/* eslint-disable react/jsx-props-no-spreading */
import { Redirect, Route } from 'react-router';
import React from 'react';

const ProtectedRoute = props => {
  const { component: Component, authUser, ...propsComponent } = props;
  const { userId } = authUser;
  console.log('props in protected', props);
  return (
    <Route
      render={renderProps => {
        if (userId) {
          return <Component {...propsComponent} />;
        }
        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: renderProps.location },
            }}
          />
        );
      }}
    />
  );
};

export default ProtectedRoute;
