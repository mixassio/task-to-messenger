import React from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { connect } from 'react-redux';
import { messagesSelector } from '../selectors';

const mapStateToProps = state => ({
  messages: messagesSelector(state),
  users: state.users,
});

const Messages = ({ messages, users }) => (
  <ListGroup>
    {messages.map(el => (
      <ListGroupItem key={el.id}>
        <i>{users[el.userId].username}</i>
        {':  '}
        <b>{el.text}</b>
      </ListGroupItem>
    ))}
  </ListGroup>
);

export default connect(mapStateToProps)(Messages);
