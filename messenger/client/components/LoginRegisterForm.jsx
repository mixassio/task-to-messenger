import React, { useState } from 'react';

const LoginForm = ({ onSubmit, title }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const handleEmailChange = event => {
    event.preventDefault();
    setEmail(event.currentTarget.value);
  };

  const handlePasswordChange = event => {
    event.preventDefault();
    setPassword(event.currentTarget.value);
  };
  const handleSubmit = async e => {
    e.preventDefault();
    const fields = { email, password };
    onSubmit(fields);
  };
  return (
    <div className="container">
      <h1>{title}</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email address</label>
          <input
            type="email"
            name="email"
            className="form-control"
            id="email"
            value={email}
            aria-describedby="emailHelp"
            onChange={handleEmailChange}
          />
          <small id="emailHelp" className="form-text text-muted">
            We will never share your email with anyone else.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            className="form-control"
            id="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

export default LoginForm;
