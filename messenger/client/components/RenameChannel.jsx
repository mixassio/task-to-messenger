import React from 'react';
import { differenceBy } from 'lodash';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Button, Modal, Toast } from 'react-bootstrap';
import { connect } from 'react-redux';
import { usersSelector } from '../selectors';
import { addUserToChannel, renameChannel } from '../actions/services';

const mapStateToProps = state => ({
  users: usersSelector(state),
});

const mapDispatchToProps = { addUserToChannel, renameChannel };

@reduxForm({ form: 'renameChannel' })
class RenameChannel extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
    };
  }

  submitMain = channelId => async value => {
    const {
      addUserToChannel: addUserAction,
      renameChannel: renameChannelAction,
      reset,
    } = this.props;
    try {
      const { user, text } = value;
      if (user) {
        await addUserAction({ channelId, userId: user });
      }
      if (text) {
        await renameChannelAction({
          name: text,
          channelId,
        });
      }
    } catch (error) {
      throw new SubmissionError({ error });
    }
    reset();
  };

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    const { show } = this.state;
    const {
      handleSubmit,
      submitting,
      channel: { id: channelId, name, users: usersInChannel },
      users,
    } = this.props;
    const usersCanAdd = differenceBy(users, usersInChannel, 'username');

    return (
      <>
        <Button size="sm" variant="outline-warning" onClick={this.handleShow}>
          <span className="oi oi-pencil" />
        </Button>

        <Modal show={show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Change channel</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h6>Rename channel</h6>
            <form
              className="form-inline mt-3"
              onSubmit={handleSubmit(this.submitMain(channelId))}
            >
              <Field
                name="text"
                placeholder={name}
                component="input"
                type="text"
                disabled={submitting}
                className="w-75 border border-info"
              />
              <hr />
              <h6>Users in channel</h6>
              <Field
                name="user"
                component="select"
                className="w-75 border border-info"
              >
                {usersCanAdd.map(({ id, username }) => (
                  <option key={id} value={id}>
                    {username}
                  </option>
                ))}
              </Field>
              <Button type="submit" variant="warning" disabled={submitting}>
                change info
              </Button>
            </form>
            {usersInChannel.map(({ userId, username }) => (
              <Toast key={userId}>
                <Toast.Header>
                  <strong className="mr-auto">{username}</strong>
                </Toast.Header>
              </Toast>
            ))}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RenameChannel);
