import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { registerNewUser } from '../actions/services';
import RegisterForm from './LoginRegisterForm';

const mapDispatchToProps = { registerNewUser };

const RegisterContainer = ({ registerNewUser: registerNewUserAction }) => {
  const [history, location] = [useHistory(), useLocation()];
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const handleSuccessAuth = () => {
    setRedirectToReferrer(true);
  };
  const onFormSubmit = async fields => {
    try {
      await registerNewUserAction(fields);
      handleSuccessAuth();
    } catch (err) {
      history.push('/registration');
    }
  };

  const { from } = location.state || { from: { pathname: '/login' } };
  console.log('from', from);

  if (redirectToReferrer === true) {
    return <Redirect to={from} />;
  }
  console.log('no redirect');

  return <RegisterForm title="Registration" onSubmit={onFormSubmit} />;
};

export default connect(null, mapDispatchToProps)(RegisterContainer);
