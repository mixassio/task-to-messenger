import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import NotFound from './NotFound';
import App from './App';
import ProtectedRoute from './ProtectedRoute';
import Login from './Login';
import NavHeader from './NavHeader';
import Register from './Register';

const mapStateToProps = ({ authUser }) => ({
  authUser,
});

const Start = props => {
  const { authUser } = props;
  return (
    <>
      <NavHeader />
      <Switch>
        <ProtectedRoute path="/" exact component={App} authUser={authUser} />
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Register} />
        <Route component={NotFound} />
      </Switch>
    </>
  );
};

export default connect(mapStateToProps)(Start);
