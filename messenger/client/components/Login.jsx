import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { loadCurrentUser, loadState } from '../actions/services';
import LoginForm from './LoginRegisterForm';

const mapDispatchToProps = { loadCurrentUser, loadState };

const LoginContainer = ({
  loadCurrentUser: loadCurrentUserAction,
  loadState: loadStateAction,
}) => {
  const [history, location] = [useHistory(), useLocation()];
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const handleSuccessAuth = () => {
    setRedirectToReferrer(true);
  };
  const onFormSubmit = async fields => {
    try {
      await loadCurrentUserAction(fields);
      await loadStateAction();
      handleSuccessAuth();
    } catch (err) {
      history.push('/login');
    }
  };

  const { from } = location.state || { from: { pathname: '/' } };
  console.log('from', from);

  if (redirectToReferrer === true) {
    return <Redirect to={from} />;
  }

  return <LoginForm title="Sign In" onSubmit={onFormSubmit} />;
};

export default connect(null, mapDispatchToProps)(LoginContainer);
