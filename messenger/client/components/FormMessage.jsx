import React from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { addMessage } from '../actions/services';

const mapStateToProps = ({ currentChannelId, authUser }) => ({
  currentChannelId,
  authUser,
});
const mapDispatchToProps = { addMessage };

@reduxForm({ form: 'newMessage' })
class FormMessage extends React.Component {
  constructor(props) {
    super(props);
    this.inputMessage = React.createRef();
  }

  componentDidMount() {
    this.inputMessage.current.getRenderedComponent().focus();
  }

  componentDidUpdate() {
    this.inputMessage.current.getRenderedComponent().focus();
  }

  submitMessage = userId => async value => {
    const {
      reset,
      addMessage: addMessageAction,
      currentChannelId,
    } = this.props;
    try {
      await addMessageAction({
        message: value.text,
        channelId: currentChannelId.current,
        userId,
      });
      reset();
    } catch (error) {
      throw new SubmissionError({ error });
    }
  };

  render() {
    const {
      handleSubmit,
      submitting,
      authUser: { userId },
    } = this.props;
    return (
      <form
        className="form-inline mt-3"
        onSubmit={handleSubmit(this.submitMessage(userId))}
      >
        <Field
          ref={this.inputMessage}
          forwardRef
          name="text"
          required
          component="input"
          type="text"
          disabled={submitting}
          className="w-75 border border-info"
        />
        <button
          type="submit"
          className="ml-1 btn btn-primary btn-sm"
          disabled={submitting}
        >
          Send
        </button>
      </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormMessage);
