import React from 'react';
import cn from 'classnames';
import { Field, reduxForm } from 'redux-form';
import { ListGroup, Button, ButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { addChannel } from '../actions/services';
import { setCurrentChannnelId } from '../actions';
import { channelsSelector } from '../selectors';
import DeleteChannel from './DeleteChannel';
import RenameChannel from './RenameChannel';

const mapStateToProps = state => ({
  channels: channelsSelector(state),
  currentChannelId: state.currentChannelId.current,
});

const mapDispatchToProps = { addChannel, setCurrentChannnelId };

@reduxForm({ form: 'newChannel' })
class Channels extends React.Component {
  setChannel = channelId => () => {
    const { setCurrentChannnelId: setCurrentChannnelIdAction } = this.props;
    setCurrentChannnelIdAction(channelId);
  };

  submitChannel = async value => {
    const { reset, addChannel: addChannelAction } = this.props;
    try {
      await addChannelAction(value.text);
      reset();
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { channels, currentChannelId, handleSubmit, submitting } = this.props;
    return (
      <ListGroup>
        <form
          className="form-inline m-1"
          onSubmit={handleSubmit(this.submitChannel)}
        >
          <Field
            name="text"
            required
            component="input"
            disabled={submitting}
            type="text"
            className="w-75 border border-info"
          />
          <button
            type="submit"
            className="ml-1 btn btn-info btn-sm"
            disabled={submitting}
          >
            NEW
          </button>
        </form>
        {channels.map(el => {
          const btnClass = cn({
            'w-100': true,
            'm-1': true,
            active: el.id === currentChannelId,
          });
          return (
            <ButtonGroup size="sm" key={el.id}>
              <Button
                size="sm"
                onClick={this.setChannel(el.id)}
                className={btnClass}
                variant="outline-success"
              >
                {el.name}
              </Button>
              {el.removable && (
                <ButtonGroup className="m-1">
                  <RenameChannel channel={el} />
                  <DeleteChannel channelId={el.id} />
                </ButtonGroup>
              )}
            </ButtonGroup>
          );
        })}
      </ListGroup>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Channels);
