const renderFullPage = (html, preloadedState = {}) => `
<!doctype html>
<html>
  <head>
    <title>Redux Universal Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  </head>
  <body>
    <div id="chat">${html}</div>
    <script>
      // WARNING: See the following for security issues around embedding JSON in HTML:
      // https://redux.js.org/recipes/server-rendering/#security-considerations
      window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
        /</g,
        '\\u003c'
      )}
      gon = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
    </script>
    <script src="/assets/main.js"></script>
  </body>
</html>
      `;

export default renderFullPage;
