const routes = {
  channels: id => `/api/v1/channels/${id}`,
  channel: () => '/api/v1/channels',
  message: id => `/api/v1/channels/${id}/messages`,
  usersChannels: id => `/api/v1/channels/${id}/users`,
  createSession: () => `/api/v1/login`,
  getState: () => `/api/v1/state`,
  logout: () => `/api/v1/logout`,
  register: () => `/api/v1/register`,
};
export default routes;
