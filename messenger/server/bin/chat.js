import getApp from '..';
import config from '../../config';

const { portServer } = config;
getApp().listen(portServer, () => console.log(`port: ${portServer}`));
