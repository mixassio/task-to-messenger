import path from 'path';
import mongoose from 'mongoose';
import Koa from 'koa';
import socket from 'socket.io';
import http from 'http';
import mount from 'koa-mount';
import serve from 'koa-static';
import Router from 'koa-router';
import koaLogger from 'koa-logger';
import koaWebpack from 'koa-webpack';
import bodyParser from 'koa-bodyparser';
import cookie from 'koa-cookie';
import favicon from 'koa-favicon';
import webpackConfig from '../webpack.dev';
import config from '../config';
import auth from './auth';
import buildApiRouter from './routes/api';
import buildAuthRouter from './routes/auth';

const { environment, secretKey, mongoUrl } = config;

const isProduction = environment === 'production';
const isDevelopment = !isProduction;

mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
mongoose.connection.on('connected', () => {
  console.log(
    `Database connection open to ${mongoose.connection.host} ${mongoose.connection.name}`
  );
});

export default () => {
  const app = new Koa();
  app.use(koaLogger());
  app.keys = [secretKey];
  app.use(cookie());
  app.use(auth());
  app.use(favicon(`${__dirname}/public/favicon.ico`));
  app.use(bodyParser());
  const server = http.createServer(app.callback());
  const io = socket(server);

  const apiRouter = new Router({ prefix: '/api/v1' });
  buildApiRouter(apiRouter, io);
  const authRouter = new Router();
  buildAuthRouter(authRouter);

  if (isDevelopment) {
    koaWebpack({
      config: webpackConfig,
      hotClient: {
        host: '0.0.0.0',
        port: 9999,
      },
    }).then(middleware => {
      app.use(middleware);
      app.use(apiRouter.allowedMethods());
      app.use(authRouter.allowedMethods());
      app.use(apiRouter.routes()).use(authRouter.routes());
    }, console.error);
  } else {
    const urlPrefix = '/assets';
    const assetsPath = path.resolve(`${__dirname}/../dist/public`);
    app.use(mount(urlPrefix, serve(assetsPath)));
    app.use(apiRouter.allowedMethods());
    app.use(authRouter.allowedMethods());
    app.use(apiRouter.routes()).use(authRouter.routes());
  }

  return server;
};
