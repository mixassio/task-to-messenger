const mongoose = require('mongoose');

const userModel = mongoose.Schema(
  {
    username: {
      type: String,
      index: true,
      unique: true,
      dropDups: true,
      required: '{PATH} is required!',
    },
    firstName: {
      type: String,
    },
    secondName: {
      type: String,
    },
    passwordHash: {
      type: String,
      required: false,
    },
    channels: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Channel' }],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('User', userModel);
