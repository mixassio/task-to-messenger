const mongoose = require('mongoose');

const messageModel = mongoose.Schema(
  {
    text: {
      type: String,
      required: '{PATH} is required!',
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    channelId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Channel',
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Message', messageModel);
