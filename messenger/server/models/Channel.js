const mongoose = require('mongoose');

const channelModel = mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
      required: '{PATH} is required!',
    },
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    removable: { type: Boolean },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Channel', channelModel);
