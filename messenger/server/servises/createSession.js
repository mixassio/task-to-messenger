import jwt from 'jsonwebtoken';
import config from '../../config';

export default async ctx => {
  const { user } = ctx.state;
  const payload = {
    id: user.id,
  };
  const token = jwt.sign(payload, config.secretKey, { expiresIn: '90d' });
  ctx.cookies.set(config.tokenCookie, token, {
    expires: new Date('2030'),
    signed: false,
  });
  ctx.body = user.id;
  ctx.status = 200;
};
