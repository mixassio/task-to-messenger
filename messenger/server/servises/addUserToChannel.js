import Channel from '../models/Channel';
import User from '../models/User';

export default io => async ctx => {
  const { channelId } = ctx.params;
  const {
    attributes: { userId },
  } = ctx.request.body.data;
  try {
    const channelUpd = await Channel.findByIdAndUpdate(
      channelId,
      { $push: { users: { _id: userId } } },
      { new: true }
    );
    const { id, name, removable, users } = channelUpd;
    const usersFull = await Promise.all(
      users.map(async userIdItem => {
        const user = await User.findById(userIdItem);
        return { id: user.id, username: user.username };
      })
    );
    const data = {
      data: {
        type: 'channels',
        id,
        attributes: { id, name, removable, users: usersFull },
      },
    };
    ctx.body = data;
    io.emit('addUserToChannel', data);
  } catch (error) {
    console.log(error);
    throw error;
  }
};
