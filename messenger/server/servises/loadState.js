import getState from '../routes/getState';

export default async ctx => {
  const {
    user: { id: userId, username },
  } = ctx.state;
  const authUser = { userId, username };
  const state = await getState(userId);
  ctx.body = { ...state, authUser };
  ctx.status = 200;
};
