import config from '../../config';

export default async ctx => {
  ctx.logout();
  ctx.cookies.set(config.tokenCookie);
  ctx.status = 200;
};
