import Channel from '../models/Channel';

export default io => async ctx => {
  const { id: userId, username } = ctx.state.user;
  const {
    data: {
      attributes: { name },
    },
  } = ctx.request.body;
  const channelDTO = {
    name,
    removable: true,
    users: [userId],
  };
  const newChannel = new Channel(channelDTO);
  await newChannel.save();
  const { id, name: nameNew, removable } = newChannel;
  const data = {
    data: {
      type: 'channels',
      id: newChannel.id,
      attributes: {
        username,
        id,
        name: nameNew,
        removable,
        users: [{ username, userId }],
      },
    },
  };
  ctx.body = data;
  ctx.status = 201;
  io.emit('newChannel', data);
};
