import Message from '../models/Message';

export default io => async ctx => {
  const {
    data: {
      attributes: { text, userId },
    },
  } = ctx.request.body;
  const messageDTO = {
    text,
    userId,
    channelId: ctx.params.channelId,
  };
  const newMessage = new Message(messageDTO);
  await newMessage.save();
  const data = {
    data: {
      type: 'messages',
      id: newMessage.id,
      attributes: {
        id: newMessage.id,
        text,
        userId,
        channelId: newMessage.channelId,
      },
    },
  };
  ctx.body = data;
  ctx.status = 201;
  io.emit('newMessage', data);
};
