/* eslint-disable no-underscore-dangle */
import bcrypt from 'bcrypt';
import Channel from '../models/Channel';
import User from '../models/User';

export default io => async ctx => {
  const { email: username, password } = ctx.request.body;
  const hashCost = 10;
  try {
    const passwordHash = await bcrypt.hash(password, hashCost);
    const user = new User({ username, passwordHash });
    await user.save();
    await Channel.findOneAndUpdate(
      { name: 'general' },
      { $push: { users: { _id: user._id } } }
    );
    await Channel.findOneAndUpdate(
      { name: 'random' },
      { $push: { users: { _id: user._id } } }
    );
    const data = {
      data: {
        type: 'users',
        id: user.id,
        attributes: { id: user.id, username: user.username },
      },
    };
    io.emit('registerNewUser', data);
    ctx.status = 201;
  } catch (error) {
    console.log(error);
    ctx.status = 400;
  }
};
