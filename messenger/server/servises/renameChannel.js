import Channel from '../models/Channel';
import User from '../models/User';

export default io => async ctx => {
  const channelId = ctx.params.id;
  const {
    attributes: { name },
  } = ctx.request.body.data;
  const { removable, users } = await Channel.findByIdAndUpdate(channelId, {
    name,
  });
  const usersFull = await Promise.all(
    users.map(async userIdItem => {
      const user = await User.findById(userIdItem);
      return { id: user.id, username: user.username };
    })
  );
  ctx.status = 204;
  const data = {
    data: {
      type: 'channels',
      id: channelId,
      attributes: {
        id: channelId,
        name,
        removable,
        users: usersFull,
      },
    },
  };
  io.emit('renameChannel', data);
};
