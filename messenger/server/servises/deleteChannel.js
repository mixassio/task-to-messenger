import Channel from '../models/Channel';
import Message from '../models/Message';

export default io => async ctx => {
  const channelId = ctx.params.id;
  await Channel.findByIdAndRemove(channelId);
  await Message.remove({ channel: channelId });
  const data = {
    data: {
      type: 'channels',
      id: channelId,
    },
  };
  ctx.status = 204;
  io.emit('removeChannel', data);
};
