import { Strategy as JWTStrategy } from 'passport-jwt';
import User from '../models/User';
import config from '../../config';

const { secretKey, tokenCookie } = config;

const opts = {
  jwtFromRequest: req => req.cookies.get(tokenCookie),
  secretOrKey: secretKey,
};

export default new JWTStrategy(opts, async (jwtPayload, done) => {
  const user = await User.findById(jwtPayload.id);
  return user ? done(null, user) : done(null, false);
});
