import { Strategy as LocalStrategy } from 'passport-local';
import bcrypt from 'bcrypt';

import UserModel from '../models/User';

export default new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
  },
  async (username, password, done) => {
    try {
      const user = await UserModel.findOne({
        username,
      }).exec();
      if (!user) {
        return done({ type: 'email', message: 'No such user found' }, false);
      }
      const passwordsMatch = await bcrypt.compare(password, user.passwordHash);
      if (!passwordsMatch) {
        return done(
          { type: 'password', message: 'Passwords did not match' },
          false
        );
      }
      return done(null, user);
    } catch (error) {
      return done(error);
    }
  }
);
