import passport from 'koa-passport';
import compose from 'koa-compose';
import UserModel from '../models/User';
import jwtStrategy from './jwt-strategy';
import localStrategy from './local-strategy';

passport.use('jwt', jwtStrategy);
passport.use('local', localStrategy);

passport.serializeUser((user, done) => {
  done(null, user.id);
});
passport.deserializeUser(async (id, done) => {
  try {
    const user = await UserModel.findById(id);
    done(null, user);
  } catch (err) {
    done(err);
  }
});

export default function auth() {
  return compose([passport.initialize(), passport.session()]);
}

export function authJWT() {
  return passport.authenticate('jwt', { failureRedirect: '/login' });
}

export function authLocal(ctx, next) {
  console.log('ku');
  return passport.authenticate(
    'local',
    {
      failureRedirect: '/login',
    },
    async (err, user) => {
      if (!user) {
        ctx.body = err;
        ctx.status = 403;
      } else {
        ctx.login(user);
        await next();
      }
    }
  );
}
