/* eslint-disable consistent-return */
import { pick } from 'lodash';
import { Channel, Message, User } from '../models';

const getState = async userId => {
  if (!userId) {
    return {
      channels: [],
      messages: [],
      users: [],
      currentChannelId: '',
    };
  }
  try {
    const users = (await User.find().exec()).map(user =>
      pick(user, ['id', 'username'])
    );
    const channels = (await Channel.find().exec())
      .filter(({ users: usersInChannel }) => usersInChannel.includes(userId))
      .map(channel => pick(channel, ['id', 'name', 'removable', 'users']))
      .map(channel => {
        const { users: usersChannel } = channel;
        const usersChannelUp = usersChannel.map(userIds => {
          const [{ username }] = users.filter(user => {
            return user.id === userIds.toString();
          });
          return { userId: userIds, username };
        });
        return { ...channel, users: usersChannelUp };
      });
    const [{ id: currentChannelId }] = channels.filter(
      ({ name }) => name === 'general'
    );
    const messages = (await Message.find().exec()).map(message =>
      pick(message, ['id', 'text', 'channelId', 'userId'])
    );
    return {
      channels,
      messages,
      users,
      currentChannelId,
    };
  } catch (err) {
    console.log(err);
  }
};

export default getState;
