/* eslint-disable arrow-parens */
import { authJWT, authLocal } from '../auth';
import {
  createUser,
  createSession,
  createChannel,
  createMessage,
  deleteChannel,
  addUserToChannel,
  logout,
  renameChannel,
  loadState,
} from '../servises';

export default async (router, io) => {
  router
    .post('/register', createUser(io))
    .post(
      '/login',
      (ctx, next) => authLocal(ctx, next)(ctx, next),
      createSession
    )
    .post('/logout', authJWT(), logout)
    .post('/state', authJWT(), loadState)
    .post('/channels', authJWT(), createChannel(io))
    .delete('/channels/:id', authJWT(), deleteChannel(io))
    .patch('/channels/:id', authJWT(), renameChannel(io))
    .post('/channels/:channelId/messages', authJWT(), createMessage(io))
    .post('/channels/:channelId/users', authJWT(), addUserToChannel(io));
  return router;
};
