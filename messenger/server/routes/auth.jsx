import * as jwt from 'jsonwebtoken';
import React from 'react';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { createStore } from 'redux';
import reducers from '../../client/reducers';
import renderFullPage from '../../client/renderFullPage';
import { Start } from '../../client/components';
import getState from './getState';
import config from '../../config';
import User from '../models/User';

const { tokenCookie } = config;

export default router => {
  router.get('root', '*', async ctx => {
    const token = ctx.cookies.get(tokenCookie);
    const authUser = { username: '', userId: '' };
    if (token) {
      const { id } = jwt.decode(token);
      const user = await User.findById(id);
      authUser.userId = id;
      authUser.username = user.username;
    }
    const initState = { authUser };
    const store = createStore(reducers, initState);

    const state = await getState(authUser.userId);

    const routerContext = {};
    const app = renderToString(
      <Provider store={store}>
        <StaticRouter location={ctx.request.url} context={routerContext}>
          <Start />
        </StaticRouter>
      </Provider>
    );
    if (routerContext.action === 'REPLACE') {
      ctx.redirect(routerContext.url);
    } else {
      ctx.body = renderFullPage(app, { ...state, authUser });
    }
  });
};
