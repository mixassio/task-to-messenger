/* eslint-disable no-underscore-dangle */
const mongoose = require('mongoose');
const Channel = require('../server/models/Channel');
const User = require('../server/models/User');
const Message = require('../server/models/Message');

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function up() {
  const channelGeneral = new Channel({ name: 'general', removable: false });
  await channelGeneral.save();
  const channelRandom = new Channel({ name: 'random', removable: false });
  await channelRandom.save();
  const userFirst = new User({ username: 'mixa@mail.ru' });
  await userFirst.save();
  await Channel.findOneAndUpdate(
    { name: 'general' },
    { $push: { users: { _id: userFirst._id } } }
  );
  await Channel.findOneAndUpdate(
    { name: 'random' },
    { $push: { users: { _id: userFirst._id } } }
  );

  const messageRandom = new Message({
    text: 'hello',
    userId: userFirst._id,
    channelId: channelRandom._id,
  });
  await messageRandom.save();
}

async function down() {
  await Channel.deleteOne({ name: 'general' });
  await Channel.deleteOne({ name: 'random' });
  await Message.deleteMany();
  await User.deleteOne({ username: 'mixa@mail.ru' });
}

module.exports = { up, down };
