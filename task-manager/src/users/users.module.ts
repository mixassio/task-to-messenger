import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users.entity';
import { ProjectModule } from '../project/project.module';
import { HookSenderModule } from '../hook-sender/hook-sender.module';

@Module({
  imports: [
    forwardRef(() => ProjectModule),
    TypeOrmModule.forFeature([User]),
    HookSenderModule,
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
