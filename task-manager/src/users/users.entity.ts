import { Column, Entity, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';
import { Project } from '../project/project.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50, unique: true })
  username: string;

  @Column({ length: 100, nullable: true, select: false })
  passwordHash: string | undefined;

  @Column({ length: 100, nullable: true })
  email: string;

  @ManyToMany(
    type => Project,
    project => project.users,
  )
  projects: Project[];
}
