import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './users.entity';
import { Project } from '../project/project.entity';
import { ProjectService } from '../project/project.service';
import { HookSenderService } from '../hook-sender/hook-sender.service';
import { CreateUsertDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  private saltRounds = 10;

  constructor(
    private readonly hookSenderService: HookSenderService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @Inject(forwardRef(() => ProjectService))
    private readonly projectService: ProjectService,
  ) {}

  // getters
  async getUsers(): Promise<User[]> {
    return await this.userRepository.find({
      relations: ['projects'],
    });
  }

  async getUserByUsername(username: string): Promise<User> {
    return await this.userRepository.findOne({ username });
  }

  async getUserByUsernameWithPassword(username: string): Promise<User> {
    const [user, _] = await this.userRepository.find({
      select: ['id', 'username', 'passwordHash', 'email'],
      where: { username },
    });

    return user;
  }

  // setters
  async createUser(createUsertDto: CreateUsertDto): Promise<number> {
    const user = this.userRepository.create();
    const { projects } = createUsertDto;
    if (projects) {
      const projectsData: Project[] = await Promise.all(
        projects.map((key: string) =>
          this.projectService.getProjectByProjectKey(key),
        ),
      );
      user.projects = projectsData.filter(Boolean);
    }
    user.username = createUsertDto.username;
    user.email = createUsertDto.email;
    user.passwordHash = await this.getHash(createUsertDto.password);
    await this.hookSenderService.sendHookData({
      dataSend: user,
      typeChange: 'new',
      typeData: 'user',
    });
    const { id } = await this.userRepository.save(user);
    return id;
  }

  // helpers
  async getHash(password: string | undefined): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  async compareHash(
    password: string | undefined,
    hash: string | undefined,
  ): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }
}
