import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class UserWasCreatedDto {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  readonly id: number;
}
