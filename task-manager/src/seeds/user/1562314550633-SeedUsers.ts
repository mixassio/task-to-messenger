import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
import { UserSeed } from './user.seed';
import { User } from '../../users/users.entity';
import { CreateUsertDto } from '../../users/dto/create-user.dto';

import * as bcrypt from 'bcrypt';

export class SeedUsers1562314550633 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const userRepository = getRepository(User);

    UserSeed.map(async (createUsertDto: CreateUsertDto) => {
      const newUser = userRepository.create();
      newUser.username = createUsertDto.username;
      newUser.email = createUsertDto.email;
      newUser.passwordHash = await bcrypt.hash(createUsertDto.password, 10);
      await userRepository.save(newUser);
    });
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const t = 'hello';
  }
}
