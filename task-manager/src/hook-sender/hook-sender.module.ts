import { Module } from '@nestjs/common';
import { HookSenderService } from './hook-sender.service';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [ConfigModule],
  providers: [HookSenderService],
  exports: [HookSenderService],
})
export class HookSenderModule {}
