import axios from 'axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { Project } from '../project/project.entity';
import { User } from '../users/users.entity';

type DataSend = Project | User;
type LifeCircle = 'new' | 'update' | 'delete';
type TypeData = 'project' | 'user' | 'task' | 'comment';

@Injectable()
export class HookSenderService {
  constructor(private readonly configService: ConfigService) {}

  async sendHookData({
    dataSend,
    typeChange,
    typeData,
  }: {
    dataSend: DataSend;
    typeChange: LifeCircle;
    typeData: TypeData;
  }): Promise<any> {
    const portHookParser = this.configService.get('PORT_HOOK_PARSER');
    const hostHookParser = this.configService.get('HOST_HOOK_PARSER');
    const rpc = axios.create({
      proxy: {
        host: hostHookParser,
        port: Number(portHookParser),
      },
    });

    try {
      const res = await rpc.post('/', {
        data: dataSend,
        typeChange,
        typeData,
        source: 'taskManager',
      });
      // tslint:disable-next-line: no-console
      console.log('res------------>>', res.status, res.statusText);
    } catch (err) {
      // tslint:disable-next-line: no-console
      console.log('error---->>>>>', err.response.data);
    }
  }
}
