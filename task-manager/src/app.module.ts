import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { AppController } from './app.controller';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ProjectModule } from './project/project.module';
import { HookSenderModule } from './hook-sender/hook-sender.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.dbParams,
    }),
    ConfigModule,
    UsersModule,
    AuthModule,
    ProjectModule,
    HookSenderModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
