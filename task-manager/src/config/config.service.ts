import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConnectionOptions, DatabaseType } from 'typeorm';

export class ConfigService {
  get(key: string): string {
    if (!process.env[key] || process.env[key] === null) {
      throw new Error(`Unknown config key ${key}`);
    }

    return process.env[key]!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
  }

  get dbParams(): TypeOrmModuleOptions {
    const env = process.env.NODE_ENV || 'development';

    const development: TypeOrmModuleOptions = {
      type: process.env.DB_TYPE as DatabaseType,
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_USER_PASSWORD,
      database: process.env.DB_DATABASE,
      synchronize: true,
      entities: ['src/**/*.entity{.ts,.js}'],
      migrationsRun: true,
      migrations: [`src/seeds/**/*{.ts,.js}`],
    } as ConnectionOptions;

    const production: TypeOrmModuleOptions = {
      type: 'postgres',
      url: process.env.DATABASE_URL,
      logging: true,
    };

    const configs: { [key: string]: TypeOrmModuleOptions } = {
      development,
      production,
    };

    return configs[env];
  }
}
