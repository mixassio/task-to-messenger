import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class ProjectsListDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly key: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  readonly taskTypes: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  readonly title: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  readonly description: string;

  @ApiProperty()
  @IsArray()
  @IsOptional()
  users: string[];
}
