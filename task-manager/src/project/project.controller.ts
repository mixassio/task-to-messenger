import {
  Controller,
  Get,
  Post,
  HttpException,
  Body,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ProjectService } from './project.service';
import { Project } from './project.entity';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectWasCreatedDto } from './dto/project-was-created';
import { ProjectsListDto } from './dto/projects-list.dto';

@ApiBearerAuth()
@ApiTags('projects')
@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @ApiOperation({ summary: 'Show all projects' })
  @ApiResponse({
    status: 201,
    description: 'All projects',
    type: ProjectsListDto,
    isArray: true,
  })
  findAll(): Promise<Project[]> {
    return this.projectService.getProjects();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'Create project' })
  @ApiResponse({
    status: 201,
    description: 'Project has been successfully created.',
    type: ProjectWasCreatedDto,
  })
  @ApiResponse({ status: 403, description: 'Project already exist.' })
  async createProject(@Body() createProjectDto: CreateProjectDto) {
    const project = await this.projectService.getProjectByProjectKey(
      createProjectDto.key,
    );

    if (project) {
      throw new HttpException('Project already exist', HttpStatus.FORBIDDEN);
    }
    return await this.projectService.createProject(createProjectDto);
  }
}
