import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from './project.entity';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.entity';
import { CreateProjectDto } from './dto/create-project.dto';
import { HookSenderService } from '../hook-sender/hook-sender.service';

@Injectable()
export class ProjectService {
  constructor(
    private readonly hookSenderService: HookSenderService,
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
  ) {}
  // getters
  async getProjects(): Promise<Project[]> {
    return await this.projectRepository.find({ relations: ['users'] });
  }

  async getProjectByProjectKey(key: string): Promise<Project> {
    return await this.projectRepository.findOne({ key });
  }
  // seters
  async createProject(createProjectDto: CreateProjectDto): Promise<Project> {
    const project = this.projectRepository.create();

    const { users } = createProjectDto;
    if (users) {
      const usersData: User[] = await Promise.all(
        users.map((username: string) =>
          this.usersService.getUserByUsername(username),
        ),
      );
      project.users = usersData.filter(Boolean);
    }

    project.key = createProjectDto.key;
    project.taskTypes = createProjectDto.taskTypes;
    project.title = createProjectDto.title;
    project.description = createProjectDto.description;
    await this.hookSenderService.sendHookData({
      dataSend: project,
      typeChange: 'new',
      typeData: 'project',
    });

    return this.projectRepository.save(project);
  }
}
