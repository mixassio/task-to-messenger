import * as request from 'supertest';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { getRepository, Repository } from 'typeorm';
import { User } from '../src/users/users.entity';
import { Project } from '../src/project/project.entity';
import createTestingApp from './app.testing';
import { loadFixtures, clearDb } from './fixtures.loader';

describe('#interview', () => {
  let app: INestApplication;
  let userRepo: Repository<User>;
  let projectRepo: Repository<Project>;
  let users;
  let projects;

  beforeEach(async () => {
    app = await createTestingApp();
    const fixtures = await loadFixtures();
    users = fixtures.User;
    projects = fixtures.Project;
    userRepo = getRepository(User);
    projectRepo = getRepository(Project);
  });
  afterEach(async () => {
    await clearDb();
  });

  it('create new interview application', async () => {
    const { kozma } = users;
    const numberOfInterviewsBefore = (await userRepo.findOne(kozma.id, {
      relations: ['projects'],
    }))!.projects.length;

    expect(numberOfInterviewsBefore).toEqual(2);
  });
});
